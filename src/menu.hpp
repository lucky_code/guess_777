#ifndef MENU_HPP
#define MENU_HPP

#include <vector>
#include <string>

void print_menu(const std::vector<std::string>&);
int ask_menu(const std::vector<std::string>&);

int biggest_word(const std::vector<std::string>&);

#endif