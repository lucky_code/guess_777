#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "game.hpp"
#include "lucky_util.hpp"
using namespace std;

bool play(Data& data) {
    Infos infos;
    int attempt;
    infos.lvl = 1;
    infos.life = 3+(infos.lvl/5);
    infos.score = 0;
    srand(time(NULL));        //Make it random
    infos.solution = gen_solution(infos.lvl); //Generates random number

    while(infos.life > 0) {
        //Start of the level
        show_infos(infos);
        cout << "Guess the number from " << green << "1" << def << " to " << green << infos.lvl*10 << def << endl << "> ";
        attempt = ask_attempt(infos);

        while(infos.life > 1 && attempt != infos.solution) {
            //Wrong answer and not dead yet
            infos.life--;

            infos.logs.push_back(attempt);

            clearscr();
            show_infos(infos);
            cout << red << "Try again.. " << def << endl;
            cout << "Guess the number from " << green << "1" << def << " to " << green << infos.lvl*10 << def << endl << "> ";
            attempt = ask_attempt(infos);
        }

        if(infos.life == 1 && attempt != infos.solution) {
            infos.life--;
            clearscr();
            show_infos(infos);
            box("You lost... The solution was " + to_string(infos.solution), "red");
        } else {
            int gain = infos.lvl*infos.life;
            infos.score+=gain;
            clearscr();
            show_infos(infos, true);
            if (infos.logs.size() <= 0) {
                box("Wow you are lucky! You guessed " + to_string(infos.solution) + " on the first try :O", "green");
            } else if (infos.logs.size() >= (3+(infos.lvl/5))-1){
                box("Almost dead! But you guessed " + to_string(infos.solution) + " on the last try", "green");
            } else {
                box("Well done! The number was " + to_string(infos.solution), "green");
            }
            log("Score: " + green.use() + "+" + to_string(gain) + def.use());
            data.add_score(infos.lvl, infos.score);
            enter();
            clearscr();
            infos.lvl++;
            infos.life=3+(infos.lvl/5);
            infos.logs.clear();
            infos.solution = gen_solution(infos.lvl);
        }
    }
    return true;
}

int gen_solution(int lvl) {
    srand(time(NULL));             //Make it random
    return rand() % (lvl*10) + 1;  //number between 1 and lvl
}

int ask_attempt(Infos infos) {
    int tmp = ask_int(1, infos.lvl*10);
    bool duplicate = false;
    for(int i=0; i<infos.logs.size(); i++) {
        if (tmp == infos.logs[i]) duplicate = true;
    }
    while(duplicate) {
        error("duplicate attempt: " + to_string(tmp));
        std::cout << "try another one: " << std::endl << "> ";
        tmp = ask_int(1, infos.lvl*10);
        duplicate = false;
        for(int i=0; i<infos.logs.size(); i++) {
            if (tmp == infos.logs[i]) duplicate = true;
        }
    }
    return tmp;
}

void show_infos(Infos infos, bool win) {
    cout << infos.solution;
    cout << blue << "° Level: " << (win ? green : def) << infos.lvl << def << endl;
    cout << blue << "° Life : " << (infos.life > 1 ? def : red) << infos.life << def << endl;
    cout << blue << "° Score: " << (win ? green : def) << infos.score << def << endl;

    for(int i = 0; i<infos.logs.size(); i++) {
        if (infos.logs[i] < infos.solution) {
            box(to_string(infos.logs[i]) + " is too small..", "blue");
        } else {
            box(to_string(infos.logs[i]) + " is too big!", "blue");
        }
    }
}