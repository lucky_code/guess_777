#ifndef LUCKY_UTIL
#define LUCKY_UTIL

#define KEY_UP 72
#define KEY_DOWN 80
#define KEY_LEFT 77
#define KEY_RIGHT 75
#define KEY_ESC 27
#define KEY_ENTER 10

#include <string>
#include <iostream>

namespace Color {
    enum Code {
        FG_RED     = 31,
        FG_GREEN   = 32,
        FG_BLUE    = 34,
        FG_DEFAULT = 39,
        BG_RED     = 41,
        BG_GREEN   = 42,
        BG_BLUE    = 44,
        BG_DEFAULT = 49
    };

    class Modifier {
        Code code;
    public:
        Modifier(Code pCode);
        void mod(Code pCode);
        Code get_code() const;
        std::string use();
    };
    std::ostream& operator<<(std::ostream&, const Modifier&);
}

static Color::Modifier def(Color::FG_DEFAULT);
static Color::Modifier red(Color::FG_RED);
static Color::Modifier green(Color::FG_GREEN);
static Color::Modifier blue(Color::FG_BLUE);

bool box(std::string);
bool box(std::string, std::string);

void clearscr();

void zzz(int);

void error(const std::string);

void log(const std::string);

void enter();

bool ask_yn(const std::string);

int ask_int(int, int);

void bye();

#endif