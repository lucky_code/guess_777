#ifndef DATA_HPP
#define DATA_HPP

#include <string>
#include <vector>

typedef struct Player {
    std::string name;
    int best_lvl = 0;
    int best_score = 0;
} Player;

class Data {
    std::string current_player;
    std::vector<Player> players;
public:
    Data();
    bool is_empty() const;
    void set_current_player(std::string);
    std::string get_current_player() const;
    Player get_player(std::string) const;
    bool add_score(int, int);
    int get_score(std::string) const;
    bool show_score() const;
    bool show_score(std::string) const;
    int get_player_index(std::string) const;
};

std::string ask_name();

#endif