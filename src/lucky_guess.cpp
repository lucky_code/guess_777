#include <iostream>
#include <unistd.h>
#include "lucky_util.hpp"
#include "data.hpp"
#include "menu.hpp"
#include "game.hpp"
using namespace std;

int main() {
    Data data;
    int action;
    vector<string> menu {};
    menu.push_back("Play");
    menu.push_back("Change player");
    menu.push_back("Show score");
    menu.push_back("About");

    clearscr();
    box("Welcome on lucky guess!", "blue");

    data.set_current_player(ask_name());

    clearscr();
    log("Logged in as " + data.get_current_player());
    log("Your best score: " + to_string(data.get_score(data.get_current_player())));
    cout << endl;

    action = ask_menu(menu);
    while (action > 0) {
        clearscr();
        switch(action) {
        case 1:
            play(data);
            break;
        case 2:
            data.set_current_player(ask_name());
            break;
        case 3:
            data.show_score();
            break;
        case 4:
            cout << blue << "lucky_guess" << endl << "coded in c++ with vscodium in Linux mint" << endl << "by " << green << "lucky777" << def << endl;
            break;
        default:
            error("wait what??");
        }
        enter();
        clearscr();
        log("Logged in as " + data.get_current_player());
        log("Your best score: " + to_string(data.get_score(data.get_current_player())));
        cout << endl;
        action = ask_menu(menu);
    }
    bye();
}