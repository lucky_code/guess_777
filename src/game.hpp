#ifndef GAME_HPP
#define GAME_HPP
#include <vector>
#include "data.hpp"

typedef struct Infos {
    int lvl;
    int life;
    int score;
    int solution;
    std::vector<int> logs;
} Infos;

bool play(Data&);
int gen_solution(int);
int ask_attempt(Infos);
void show_infos(Infos, bool = false);

#endif