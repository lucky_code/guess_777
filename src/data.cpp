#include <iostream>
#include <iomanip>
#include <vector>
#include "lucky_util.hpp"
#include "data.hpp"
using namespace std;

Data::Data() : players(vector<Player> {}) { }

bool Data::is_empty() const {
    return this->players.empty();
}

void Data::set_current_player(std::string name) {
    this->current_player = name;
}

std::string Data::get_current_player() const {
    return this->current_player;
}

bool Data::add_score(int lvl, int score) {
    int index = get_player_index(this->current_player);
    int cpt;

    std::cout << index << endl;

    if (index < 0) {
        cpt = 0;
        
        while(cpt < this->players.size() && score <= this->players[cpt].best_score) {
            cpt++;
            std::cout << "1";
        }

        index = cpt;

        if (cpt == this->players.size()) {
            //Adds at the end of the list (worst score)
            this->players.push_back(Player());
            std::cout << "2";
        } else {
            //Inserts at the right position (descending order)
            this->players.insert(this->players.begin() + index, Player());
            std::cout << "3";
        }
    } else {
        cpt = 0;

        while(cpt < this->players.size() && score <= this->players[cpt].best_score) {
            cpt++;
            std::cout << "4";
        }

        if (cpt < index) {
            this->players.insert(this->players.begin() + cpt, this->players[index]);
            this->players.erase(this->players.begin() + (index+1));
            index = cpt;
            std::cout << "5";
        }
    }

    this->players[index].name = this->current_player;
    if (lvl > this->players[index].best_lvl) {
        this->players[index].best_lvl = lvl;
    }
    if (score > this->players[index].best_score) {
        this->players[index].best_score = score;
        box("New high score!", "green");
    }

    log("Score saved.");
    return true;
}

int Data::get_score(string name) const {
    int index = get_player_index(name);

    if(index < 0) {
        return 0;
    }

    return this->players[index].best_score;
}

bool Data::show_score() const {
    int ms=42;
    bool me;
    cout << endl << "       ╒═══════════════════════╤════════════════════════╤════════════════════════╕" << endl;
    zzz(ms);
    cout << "       │" << setw(13) << "NAME" << "          │   "
         << setw(14) << "BEST LEVEL" << "       │   "
         << setw(14) << "BEST SCORE" << "       │░" << endl
         << "       ╞═══════════════════════╪════════════════════════╪════════════════════════╡░" << endl;
    zzz(ms);

    for (int i=0; i<this->players.size(); i++) {
        me = (this->current_player == this->players[i].name);
        cout << "       │  " << (me ? green : def) << setw(17) << this->players[i].name << def << "    │   "
             << (me ? green : def) << setw(10) << this->players[i].best_lvl << def << "           │      "
             << (me ? green : def) << setw(11) << this->players[i].best_score << def << "       │░" << endl;
        zzz(ms);
    }
    cout << "       └───────────────────────┴────────────────────────┴────────────────────────┘░" << endl;
    zzz(ms);
    cout << "         ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░" << endl << endl;
    zzz(ms);
    if (is_empty()) {
        cout << red << "Score list is empty" << def << endl;
    }
    return true;
}

bool Data::show_score(string name) const {
    int index = get_player_index(name);

    if(index < 0) {
        cout << "Error: '" << name << "' not found in score list!" << endl;
        return false;
    }

    cout << "Name: " << this->players[index].name << "\tBest score: " << this->players[index].best_score << endl;
    return true;
}

int Data::get_player_index(string name) const {
    int index = 0;
    
    while (index < this->players.size() && this->players[index].name != name) {
        index++;
    }

    if (index < this->players.size()) {
        return index;
    }

    return -1;
}

string ask_name() {
    string name;
    cout << "Enter your nickname: ";
    cin >> name;
    while(name.length() > 16) {
        error("max 16 characters!");
        cout << "Enter your nickname: ";
        cin >> name;
    }

    while(!ask_yn(blue.use() + name + def.use() + " is correct?")) {
        cout << "Enter your nickname: ";
        cin >> name;
        while(name.length() > 16) {
            error("max 16 characters!");
            cout << "Enter your nickname: ";
            cin >> name;
        }
    }
    log(name + " created");
    box("Welcome " + name + "!", "green");
    return name;
}