#include <iostream>
#include "lucky_util.hpp"
#include "menu.hpp"

void print_menu(const std::vector<std::string>& menu) {
    int big = biggest_word(menu);
    int ms = 42;
    if (big < 4) big = 4;

    //TOP

    std::cout << "       ╒";
    for (int i=0; i<big+7; i++) {
        std::cout << "═";
    }
    std::cout << "╕" << std::endl;
    zzz(ms);

    //EMPTY

    std::cout << "       │";
    for (int i=0; i<big+7; i++) {
        std::cout << " ";
    }
    std::cout << "│░" << std::endl;
    zzz(ms);

    //MENU

    std::cout << "       │";
    for (int i=0; i<(big/2) + 2; i++) {
        std::cout << " ";
    }
    std::cout << blue << "MENU" << def;
    for (int i=0; i<(big/2)+1+(big%2); i++) {
        std::cout << " ";
    }
    std::cout << "│░" << std::endl;
    zzz(ms);

    //EMPTY

    std::cout << "       │";
    for (int i=0; i<big+7; i++) {
        std::cout << " ";
    }
    std::cout << "│░" << std::endl;
    zzz(ms);

    //ENTRIES

    for(int i=0; i<menu.size(); i++) {
        std::cout << "       │ " << (i+1) << ".  ";
        std::cout << menu[i];
        for(int j=0; j<big-menu[i].length()+2; j++) {
            std::cout << " ";
        }
        std::cout << "│░" << std::endl;
        zzz(ms);
    }

    //QUIT
    
    std::cout << "       │ " << blue << "0.  Quit" << def;
    for(int j=0; j<big-2; j++) {
        std::cout << " ";
    }
    std::cout << "│░" << std::endl;
    zzz(ms);

    //EMPTY

    std::cout << "       │";
    for (int i=0; i<big+7; i++) {
        std::cout << " ";
    }
    std::cout << "│░" << std::endl;
    zzz(ms);

    //BOTTOM
    std::cout << "       └";
    for (int i=0; i<big+7; i++) {
        std::cout << "─";
    }
    std::cout << "┘░" << std::endl;
    std::cout << "         ";
    for (int i=0; i<big+7; i++) {
        std::cout << "░";
    }
    std::cout << "░" << std::endl << std::endl;
    zzz(ms);
}

int ask_menu(const std::vector<std::string>& menu) {
    std::string choice;
    print_menu(menu);
    std::cout << "What do you want to do? " << blue << "[0-" << menu.size() << "]" << def << " > ";
    std::cin >> choice;

    while ((choice.length() > 1) || (choice[0]-48) < 0 || (choice[0]-48) > menu.size()) {
        std::string err;
        if (choice.length() > 1) {
            err = choice;
        } else {
            err = (choice[0]-48 >= 0 && choice[0]-48 <= 9) ? (std::to_string(choice[0]-48)) : (choice);
        }
        error("'" + err + "' is invalid (0 to " + std::to_string(menu.size()) + ")");
        std::cout << "What do you want to do? " << blue << "[0-" << menu.size() << "]" << def << " > ";
        std::cin >> choice;
    }

    return choice[0]-48;
}

int biggest_word(const std::vector<std::string>& menu) {
    int result = 0;

    for(int i=0; i<menu.size(); i++) {
        if (menu[i].length() > result) result = menu[i].length();
    }

    return result;
}