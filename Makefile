#NAME  : lucky_guess
#AUTHOR: lucky777
#HOWTO : make && make clean

objects = lucky_guess.o lucky_util.o data.o menu.o game.o

lucky_guess: $(objects)
	@echo "assembling lucky_guess.."
	@g++ -o lucky_guess $(objects)
	@echo "lucky_guess created !"

lucky_guess.o: src/lucky_guess.cpp
	@echo "compiling lucky_guess.cpp .."
	@g++ -c $<
	@echo "ok"

lucky_util.o: src/lucky_util.cpp
	@echo "compiling lucky_util.cpp .."
	@g++ -c $<
	@echo "ok"

data.o: src/data.cpp
	@echo "compiling data.cpp .."
	@g++ -c $<
	@echo "ok"

menu.o: src/menu.cpp
	@echo "compiling menu.cpp .."
	@g++ -c $<
	@echo "ok"

game.o: src/game.cpp
	@echo "compiling game.cpp .."
	@g++ -c $<
	@echo "ok"

.PHONY: clean
clean:
	@echo "cleaning lucky_guess.."
	@rm -f lucky_guess *.o *~
	@echo "removed!"

.PHONY: run
run: lucky_guess
	@echo "starting lucky_guess.."
	@./$<